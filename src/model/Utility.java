package model;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import database.Database;
import model.HelperCocktail.Cocktail;
import model.HelperIngrediente.Ingrediente;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Utility {
	
	public static List<Cocktail> ajaxCocktail(String s){
		OkHttpClient client = new OkHttpClient().newBuilder().readTimeout(0, TimeUnit.MILLISECONDS).build();
		List<Cocktail> listaCocktail=new CopyOnWriteArrayList<Cocktail>();
		String help=null;
		for(char i='a';i<'z';i++) {
			Request req = new Request.Builder()
					.url(s+i)
					.method("GET", null) 
					.build();
			try (Response res = client.newCall(req).execute()) {
				if(res.code()!=200) System.out.println("C'� un problema col server");
				else {
					help= res.body().string();
					res.body().close();
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			List<Cocktail> c=new GsonBuilder().create().fromJson(help, HelperCocktail.class).getCocktails();
			if(c!=null) {
				listaCocktail.addAll(c);
			}
		}return listaCocktail;
	}
	
	public static List<Ingrediente> ajaxIngredienti(List<String> listaIngString, String s){
		OkHttpClient client = new OkHttpClient().newBuilder().readTimeout(0, TimeUnit.MILLISECONDS).build();
		String ingredientiObj=null;
		List<Ingrediente> listaIngredienti= new CopyOnWriteArrayList<Ingrediente>();
		for(int i=0;i<listaIngString.size();i++) {
			Request req1 = new Request.Builder()
					.url(s+listaIngString.get(i))
					.method("GET", null) 
					.build();
			try (Response res = client.newCall(req1).execute()) {
				if(res.code()!=200) System.out.println("C'� un problema col server");
				else {
					ingredientiObj= res.body().string();
					Ingrediente x=new GsonBuilder().create().fromJson(ingredientiObj, HelperIngrediente.class).getIngredients();
					if(!listaIngredienti.contains(x)) listaIngredienti.add(x);
					res.body().close();
				}
				res.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}return listaIngredienti;
	}
	
	public static List<String> prendiCategorie(List<Cocktail> l){
		List<String> lista=new ArrayList<String>();
		for(int x=0;x<l.size();x++) {
			if(!lista.contains(l.get(x).getStrCategory())&&!l.get(x).getStrCategory().equals("")) {
				lista.add(l.get(x).getStrCategory());
			}
		}return lista;
	}
	public static List<String> prendiIngredienti(List<Cocktail> l){
		List<String> lista=new ArrayList<String>();
		for(int x=0;x<l.size();x++) {
			for(String c: l.get(x).getIngredienti()) {
				if(!lista.contains(c)&&!c.equals("")) {
					lista.add(c.trim());
				}
			}
		}return lista;
	}
	
	public static List<String> prendiBicchieri(List<Cocktail> l){
		List<String> lista=new ArrayList<String>();
		for(int x=0;x<l.size();x++) {
			String glass= l.get(x).getStrGlass(); 
			if(!lista.contains(glass)&&!glass.equals("")) 
				lista.add(glass.trim());
		}return lista;
	}

	public static List<String> generateInizialiCocktail(List<Cocktail> l){
		List<String> lista=new ArrayList<String>();
		for(int x=0;x<l.size();x++) {
			if(!lista.contains(""+l.get(x).getStrDrink().charAt(0))) {
				lista.add(""+l.get(x).getStrDrink().toUpperCase().charAt(0));
			}
		}
		return lista;
	}
	public static List<String> generateInizialiIngredienti(List<Ingrediente> l){
		List<String> lista=new ArrayList<String>();
		for(int x=0;x<l.size();x++) {
			if(!lista.contains(""+l.get(x).getStrIngredient().charAt(0))) {
				lista.add(""+l.get(x).getStrIngredient().toUpperCase().charAt(0));
			}
		}
		return lista;
	}
	
	public static Cocktail getRandomCocktail(ZoneId z) {
		String s= z.toString().replaceAll("/", "-");
		Path p= Paths.get("elenco/"+s+".txt");
		if(Files.exists(p)) {
			try {
				List<String> testo= Files.readAllLines(p);
				String json="";
				for(String q:testo) json+=q;
				CustomSavedCocktail c= new Gson().fromJson(json, CustomSavedCocktail.class);
				if(LocalDate.now(z).equals(c.getData())) {
					return c.getCocktail();
				}else {
					Files.deleteIfExists(p);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return getNewRandomCocktail(z);
	}
	
	private static Cocktail getNewRandomCocktail(ZoneId z) {
		Path p= Paths.get("elenco");
		if(Files.notExists(p)) {
			try {
				Files.createDirectory(p);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String zone= z.toString().replaceAll("/", "-");
		p=p.resolve(zone+".txt");
		List<Cocktail> list=Database.getInstance().getAllCocktails();
		Random rand = new Random();
		Cocktail randomElement = list.get(rand.nextInt(list.size()-1));
		CustomSavedCocktail cs= new CustomSavedCocktail(LocalDate.now(z), randomElement);
		try {
			Files.deleteIfExists(p);
			Files.createFile(p);
			String json= new Gson().toJson(cs);
			OpenOption o= StandardOpenOption.APPEND;
			BufferedWriter bw= Files.newBufferedWriter(p,o);
			bw.write(json);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cs.getCocktail();
	}
	
	private static class CustomSavedCocktail{
		private LocalDate data;
		private Cocktail cocktail;
		public CustomSavedCocktail(LocalDate data, Cocktail cocktail) {
			super();
			this.data = data;
			this.cocktail = cocktail;
		}
		public LocalDate getData() {
			return data;
		}
		public void setData(LocalDate data) {
			this.data = data;
		}
		public Cocktail getCocktail() {
			return cocktail;
		}
		public void setCocktail(Cocktail cocktail) {
			this.cocktail = cocktail;
		}
		public String getJsonObject() {
			return new Gson().toJson(this);
		}
		
	}
}
