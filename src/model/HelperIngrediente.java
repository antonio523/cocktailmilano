package model;

import java.util.Arrays;
import java.util.List;

public class HelperIngrediente {
	private Ingrediente[] ingredients;
	
	public Ingrediente getIngredients(){
		return ingredients[0];
	}
	/*
	 * Creo una inner class per mappare il json dell'ingrediente in quanto � strutturato come un oggetto 
	 * con all'interno una lista contenente un singolo oggetto che rappresenta l'ingrediente.
	 */
	public class Ingrediente{
		private int idIngredient;
		private String strIngredient;
		private String strDescription;
		private String strType;
		private int counter;
		private String immagine;
		
		public String getImmagine() {
			return immagine;
		}
		public void setImmagine() {
			this.immagine="https:/www.thecocktaildb.com/images/ingredients/"+this.strIngredient.trim()+"-Medium.png";
		}
		public int getIdIngredient() {
			return idIngredient;
		}
		public void setIdIngredient(int idIngredient) {
			this.idIngredient = idIngredient;
		}
		public String getStrIngredient() {
			return strIngredient;
		}
		public void setStrIngredient(String strIngredient) {
			this.strIngredient = strIngredient;
		}
		public String getStrDescription() {
			return strDescription;
		}
		public void setStrDescription(String strDescription) {
			this.strDescription = strDescription;
		}
		public String getStrType() {
			return strType;
		}
		public void setStrType(String strType) {
			this.strType = strType;
		}
		public int getCounter() {
			return counter;
		}
		public void setCounter() {
			this.counter++;
		}
		public boolean equals(Object o) {
			if(o==null) return false;
			if(!(o instanceof Ingrediente)) return false;
			Ingrediente i= (Ingrediente)o;
			return (i.getIdIngredient()==getIdIngredient());
		}
	}
}
