package database;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.HelperCocktail.Cocktail;
import model.HelperIngrediente.Ingrediente;
import model.Utility;
/*
 * PUNTO DI AVVIO
 * Chiamo i metodi principali ajaxCocktail/ajaxIngredienti che mappano i json ottenuti dalla repo in oggetti personalizzati delle classi Cocktail/Ingrediente
 * e restituiscono una lista.
 * 
 * Chiamo i metodi che da queste liste mi estrapolano dati quali bicchieri, categorie e restituiscono una lista.
 * Aggiungo tutte le liste ottenute alle liste contenute nel singleton della classe Database
 *  
 *  */

@WebServlet("/SetterDatabase")
public class SetterDatabase extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathIngredientiForName="https://www.thecocktaildb.com/api/json/v1/1/search.php?i="; 
		String PathCocktail="https://www.thecocktaildb.com/api/json/v1/1/search.php?f=";
		//ritorna tutti i cocktail dalle Api 
		List<Cocktail> listaCocktail= Utility.ajaxCocktail(PathCocktail); 
		//ritorna i nomi degli ingredienti dai cocktail
		List<String> stringIngredients= Utility.prendiIngredienti(listaCocktail); 
		//ritorna gli ingredienti dalle Api
		List<Ingrediente> listaIngredienti= Utility.ajaxIngredienti(stringIngredients, pathIngredientiForName); 
		//ritorna la lista dei nomi di categorie dai cocktail
		List<String> listaCategorie= Utility.prendiCategorie(listaCocktail);
		//ritorna la lista dei tipi di bicchiere dalla lista dei cocktail
		List<String> listaBicchieri=Utility.prendiBicchieri(listaCocktail);
		listaCocktail.forEach(k->k.setIngredientiAlCocktail()); 
		/*Le api ritornano i cocktail con gli ingredienti come attributi separati, questo metodo li ragguppa tutti in una lista all'interno dei singoli cocktail*/
		listaCocktail.forEach(k->k.setImgIngredienti());
		listaIngredienti.forEach(k->k.setImmagine());
		Database.getInstance().addCategories(listaCategorie);
		Database.getInstance().addIngredients(Collections.synchronizedList(listaIngredienti));
		Database.getInstance().addCocktails(Collections.synchronizedList(listaCocktail));
		Database.getInstance().addIngredientsName(stringIngredients);
		Database.getInstance().setInizialiCocktail(listaCocktail);
		Database.getInstance().setInizialiIngredienti(listaIngredienti);
		Database.getInstance().setGlasses(listaBicchieri);
	}
}
