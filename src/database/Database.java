package database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import model.HelperCocktail.Cocktail;
import model.HelperIngrediente.Ingrediente;
import model.Utility;;
/*
 * Singleton contentente tutte le liste necessarie con i relativi getter e setter (le liste vengono riempite all'avvio del server dalla classe setterDatabase)
 */
public class Database {
	private List<String> allIngredientsName;
	private List<Cocktail> allCocktail;
	private List<Ingrediente> allIngredients;
	private List<String> allCategories;
	private List<String> inizialiCocktail;
	private List<String> inizialiIngredienti;
	private List<String> allGlasses;

	private static Database instance= new Database();
	private Database() {}

	
	public List<String> getAllGlasses() {
		return allGlasses;
	}

	public void setGlasses(List<String> allGlasses) {
		this.allGlasses = allGlasses;
	}

	public List<String> getInizialiCocktail() {
		Collections.sort(inizialiCocktail);
		return inizialiCocktail;
	}

	public void setInizialiIngredienti(List<Ingrediente> ingredienti) {
		this.inizialiIngredienti = Utility.generateInizialiIngredienti(ingredienti);
	}
	public List<String> getInizialiIngredienti() {
		Collections.sort(inizialiIngredienti);
		return inizialiIngredienti;
	}

	public void setInizialiCocktail(List<Cocktail> cocktails) {
		this.inizialiCocktail = Utility.generateInizialiCocktail(cocktails);
	}
	public List<Cocktail> getAllCocktails(){
		return allCocktail;
	}
	public List<Ingrediente> getAllIngredients(){
		return allIngredients;
	}
	public static Database getInstance() {
		return instance;
	}
	public void addCocktails(List<Cocktail> cocktails) {
		this.allCocktail=cocktails;
	}
	public void addIngredients(List<Ingrediente> ing) {
		this.allIngredients=ing;
	}
	public void addIngredientsName(List<String> ing) {
		this.allIngredientsName=ing;
	}
	public void addCategories(List<String> cat) {
		this.allCategories=cat;
	}



	public List<Cocktail> getCocktailByLetter(String c){	
		List<Cocktail> cocktailByLetter=new ArrayList<>();
		List<Cocktail> a= allCocktail;
		for(int x=0;x<a.size();x++) {
			if(a.get(x).getStrDrink().toLowerCase().charAt(0)==c.toLowerCase().charAt(0)) {
				cocktailByLetter.add(a.get(x));
			}
		}return cocktailByLetter;
	}
	public List<Ingrediente> getIngredienteByLetter(String c){	
		List<Ingrediente> ingredientByLetter=new ArrayList<>();
		List<Ingrediente> a= allIngredients;
		for(int x=0;x<a.size();x++) {
			if(a.get(x).getStrIngredient().toLowerCase().charAt(0)==c.toLowerCase().charAt(0)) {
				ingredientByLetter.add(a.get(x));
			}
		}return ingredientByLetter;
	}
	public Cocktail getCocktailByName(String cocktail) {
		Cocktail cocktailByName=null;
		List<Cocktail> a= allCocktail;
		for(int i=0;i<a.size();i++) {
			if(a.get(i).getStrDrink().trim().equalsIgnoreCase(cocktail.trim())) {
				cocktailByName=a.get(i);
			}
		}
		return cocktailByName;
	}
	public synchronized  Cocktail getCocktailById(int cocktailId) {
		Cocktail cocktailByName=null;
		List<Cocktail> a= allCocktail;
		for(int i=0;i<a.size();i++) {
			if(a.get(i).getIdDrink()==cocktailId) {
				cocktailByName=a.get(i);
			}
		}
		return cocktailByName;
	}
	public List<String> getAllCategories(){
		return allCategories;
	}


	public List<Cocktail> getCocktailAlcolici() {
		List<Cocktail> cocktailAlcolici=new ArrayList<>();
		List<Cocktail> a= allCocktail;
		for(int i=0;i<a.size();i++) {
			if(a.get(i).getStrAlcoholic().equalsIgnoreCase("Alcoholic")) {
				cocktailAlcolici.add(a.get(i));
			}
		}
		return cocktailAlcolici;
	}
	public List<Cocktail> getCocktailAnalcolici() {
		List<Cocktail> cocktailAnalcolici=new ArrayList<>();
		List<Cocktail> a= allCocktail;
		for(int i=0;i<a.size();i++) {
			if(a.get(i).getStrAlcoholic().equalsIgnoreCase("Non alcoholic")) {
				cocktailAnalcolici.add(a.get(i));
			}
		}
		return cocktailAnalcolici;
	}
	public Ingrediente getIngredientiByName(String ingrediente){
		Ingrediente ingredientiByName=null;
		List<Ingrediente> a= allIngredients;
		for(int i=0;i<a.size();i++) {
			if(a.get(i).getStrIngredient().trim().equalsIgnoreCase(ingrediente)) {
				ingredientiByName=a.get(i);
			}
		}
		return ingredientiByName;
	}
	public synchronized  List<Cocktail> byAlchol(String alcolico){
		List<Cocktail> cocktailForAlcholic= new ArrayList<>();
		if(alcolico!=null) {
			cocktailForAlcholic=alcolico.equalsIgnoreCase("Alcoholic")? getCocktailAlcolici():getCocktailAnalcolici();
		}
		return cocktailForAlcholic;
	}
	
	public List<Cocktail> byCategory(String category){
		List<Cocktail> data= allCocktail;
		List<Cocktail> cocktailForCategory= new ArrayList<>();
		if(category!=null) {
			for(int i=0;i<data.size();i++) {
				if(data.get(i).getStrCategory()!=null&&data.get(i).getStrCategory().trim().equalsIgnoreCase(category)) {
					cocktailForCategory.add(data.get(i));
				}
			}
		}
		return cocktailForCategory;
	}
	public List<Cocktail> byIngredient(String... b ){
		List<Cocktail> a= allCocktail;
		List<Cocktail> list= new ArrayList<>();
		if(b!=null) {
			for(int x=0;x<a.size();x++) {
				if(b.length==1) {
					if(a.get(x).getIngredienti().contains(b[0])) { 
						Database.getInstance().getIngredientiByName(b[0]);
						list.add(a.get(x));
					}
				}else if(b.length==2) {
					if(a.get(x).getIngredienti().contains(b[0])&& a.get(x).getIngredienti().contains(b[1])) {
						Database.getInstance().getIngredientiByName(b[0]);
						Database.getInstance().getIngredientiByName(b[1]);
						list.add(a.get(x));
					}
				}else if(b.length==3) {
					if(a.get(x).getIngredienti().contains(b[0])&& a.get(x).getIngredienti().contains(b[1])&& a.get(x).getIngredienti().contains(b[2])) { //Da continuare domani
						Database.getInstance().getIngredientiByName(b[0]);
						Database.getInstance().getIngredientiByName(b[1]);
						Database.getInstance().getIngredientiByName(b[2]);
						list.add(a.get(x));
					}
				}
			}
		}
		return list;
	}
	
	public List<Cocktail> byGlass(String inputGlass){
		List<Cocktail> a= allCocktail;
		List<Cocktail> list= new ArrayList<Cocktail>();
		if(inputGlass!=null) {
			for(int x=0;x<a.size();x++) {
				if(a.get(x).getStrGlass().equalsIgnoreCase(inputGlass)) list.add(a.get(x));
			}
		}
		return list;
	}
}
