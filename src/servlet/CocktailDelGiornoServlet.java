package servlet;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import database.Database;
import model.HelperCocktail.Cocktail;
import model.Utility;


@WebServlet("/cocktailDelGiornoServlet.json")
public class CocktailDelGiornoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Cocktail> all= Database.getInstance().getAllCocktails();
		Database db= Database.getInstance();
		Locale cliente = request.getLocale();
		Calendar calendar=Calendar.getInstance(cliente);
		TimeZone clienteTimeZone= calendar.getTimeZone();
		ZoneId tz= clienteTimeZone.toZoneId();
		Cocktail cocktailGiorno= Utility.getRandomCocktail(tz);
		response.setContentType("application/json");
		String jsonComposto=null;  
		Gson g=new Gson(); 
		jsonComposto=g.toJson(cocktailGiorno);
		response.getWriter().append(jsonComposto);
//		}
	}
}
