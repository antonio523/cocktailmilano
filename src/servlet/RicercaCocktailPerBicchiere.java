package servlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import database.Database;
import model.HelperCocktail.Cocktail;


@WebServlet("/ricercaCocktailPerBicchiereServlet.json")
public class RicercaCocktailPerBicchiere extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String c=request.getParameter("type");
		List<Cocktail> lista= Database.getInstance().byGlass(c);
		response.setContentType("application/json");
		String jsonComposto=null;
		Gson gs= new Gson();
		jsonComposto= gs.toJson(lista);
		response.getWriter().append(jsonComposto);
	}
}
