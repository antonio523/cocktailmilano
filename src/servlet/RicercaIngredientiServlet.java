package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import database.Database;
import model.HelperIngrediente.Ingrediente;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@WebServlet("/ricercaIngredienti.json")
public class RicercaIngredientiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String c=request.getParameter("type");
		c=c.trim();
		if(c!=null) {
			Ingrediente all=Database.getInstance().getIngredientiByName(c);
			all.setCounter();
			response.setContentType("application/json");
			String jsonComposto=null;
			Gson gs= new Gson();
			jsonComposto= gs.toJson(all);
			response.getWriter().append(jsonComposto);
		}
	}
}
