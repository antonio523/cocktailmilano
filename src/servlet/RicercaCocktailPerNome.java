package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import database.Database;
import model.HelperCocktail.Cocktail;

/**
 * Servlet implementation class RicercaCocktailPerNome
 */
@WebServlet("/ricercaCocktailPerNome.json")
public class RicercaCocktailPerNome extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String c=request.getParameter("type");
		//c=c.trim();
		//if(c!=null) {
			Cocktail name =Database.getInstance().getCocktailByName(c);
			if(name!=null) name.setCounter();
			response.setContentType("application/json");
			String jsonComposto=null;
			Gson gs= new Gson();
			jsonComposto= gs.toJson(name);
			response.getWriter().append(jsonComposto);
		//}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
