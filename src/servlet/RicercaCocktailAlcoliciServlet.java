package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import database.Database;
import model.HelperCocktail.Cocktail;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


@WebServlet("/ricercaCocktailAlcoliciServlet.json")
public class RicercaCocktailAlcoliciServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			List<Cocktail> alcolici= Database.getInstance().getCocktailAlcolici();
			response.setContentType("application/json");
			String jsonComposto=null;
			Gson gs= new Gson();
			jsonComposto= gs.toJson(alcolici);
			response.getWriter().append(jsonComposto);
	}
}
