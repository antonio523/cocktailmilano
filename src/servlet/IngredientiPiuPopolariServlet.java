package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.filters.CorsFilter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;


import database.Database;
import model.HelperCocktail.Cocktail;
import model.HelperIngrediente.Ingrediente;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@WebServlet("/ingredientiPiuPopolariServlet.json")
public class IngredientiPiuPopolariServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * Ritorna i primi 10 ingredienti piu ricercati dal client
		 * 
		 * */
		List<Ingrediente> data= Database.getInstance().getAllIngredients();
		data.sort((a,b)->b.getCounter()-a.getCounter());
		data=data.subList(0, 8);
		response.setContentType("application/json");
		String jsonComposto=null;
		Gson g=new Gson();
		jsonComposto=g.toJson(data);
		response.getWriter().append(jsonComposto);
	}
}