package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import database.Database;
import model.HelperCocktail.Cocktail;
import model.HelperIngrediente.Ingrediente;

/**
 * Servlet implementation class RicercaIngredienteAlfabetica
 */
@WebServlet("/ingredienteRicercaAlfabetica.json")
public class RicercaIngredienteAlfabetica extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lettera= request.getParameter("iniziale");
		List<Ingrediente> list=Database.getInstance().getIngredienteByLetter(lettera);
		response.setContentType("application/json");
		String jsonComposto=null;
		Gson gs= new Gson();
		jsonComposto= gs.toJson(list);
		response.getWriter().append(jsonComposto);
	}
}
