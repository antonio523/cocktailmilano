package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.Utilities;

import com.google.gson.Gson;

import database.Database;
import model.HelperCocktail.Cocktail;
import model.Utility;


@WebServlet("/cocktailPiuPopolariServlet.json")
public class CocktailPiuPopolariServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			List<Cocktail> data= Database.getInstance().getAllCocktails();
			data.sort((a,b)->b.getCounter()-a.getCounter());
			data=data.subList(0, 8);
			response.setContentType("application/json");
			String jsonComposto=null;
			Gson gs= new Gson();
			jsonComposto= gs.toJson(data);
			response.getWriter().append(jsonComposto);
	}
}
