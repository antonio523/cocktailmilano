package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.filters.CorsFilter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;


import database.Database;
import model.HelperCocktail.Cocktail;
import model.HelperIngrediente.Ingrediente;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


@WebServlet("/randomCocktailServlet.json")
public class RandomCocktailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * Ritorna un cocktail randomico
		 */
		List<Cocktail> list=Database.getInstance().getAllCocktails();
		Random rand = new Random();
		Cocktail randomElement = list.get(rand.nextInt(list.size()-1));
		response.setContentType("application/json");
		String jsonComposto=null;
		Gson g=new Gson();
		jsonComposto=g.toJson(randomElement);
		response.getWriter().append(jsonComposto);
	}
}