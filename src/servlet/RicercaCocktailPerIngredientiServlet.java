package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import database.Database;
import model.HelperCocktail.Cocktail;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Servlet implementation class RicercaCocktailPerIngredientiServlet
 */
@WebServlet("/ricercaCocktailPerIngredientiServlet.json")
public class RicercaCocktailPerIngredientiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Database db= Database.getInstance();
		String c=request.getParameter("ing1");
		String d=request.getParameter("ing2");
		String e=request.getParameter("ing3");
			List<Cocktail> ingrediente= new ArrayList<>();
			if(d==null&&e==null) {
				ingrediente= db.byIngredient(c.toUpperCase());
			}
			else if(e==null) {
				ingrediente= db.byIngredient(c.toUpperCase(),d.toUpperCase());
			}
			else ingrediente= db.byIngredient(c.toUpperCase(),d.toUpperCase(),e.toUpperCase());
			response.setContentType("application/json");
			String jsonComposto=null;
			Gson gs= new Gson();
			jsonComposto= gs.toJson(ingrediente);
			response.getWriter().append(jsonComposto);
	}
//	public List<Cocktail> byIngredient(List<Cocktail> a, String... b ){
//		List<Cocktail> list= new ArrayList<>();
//		for(int x=0;x<a.size();x++) {
//			if(b.length==1) {
//				if(a.get(x).getIngredienti().contains(b[0])) { 
//					Database.getInstance().getIngredientiByName(b[0]).setCounter();
//					list.add(a.get(x));
//				}
//			}else if(b.length==2) {
//				if(a.get(x).getIngredienti().contains(b[0])&& a.get(x).getIngredienti().contains(b[1])) {
//					Database.getInstance().getIngredientiByName(b[0]).setCounter();
//					Database.getInstance().getIngredientiByName(b[1]).setCounter();
//					list.add(a.get(x));
//				}
//			}else if(b.length==3) {
//				if(a.get(x).getIngredienti().contains(b[0])&& a.get(x).getIngredienti().contains(b[1])&& a.get(x).getIngredienti().contains(b[2])) { //Da continuare domani
//					Database.getInstance().getIngredientiByName(b[0]).setCounter();
//					Database.getInstance().getIngredientiByName(b[1]).setCounter();
//					Database.getInstance().getIngredientiByName(b[2]).setCounter();
//					list.add(a.get(x));
//				}
//			}
//		}return list;
//	}
}
