package servlet;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import database.Database;
import model.HelperCocktail.Cocktail;

/**
 * Servlet implementation class allCategorie
 */

@WebServlet("/allCategorie.json")
public class allCategorie extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> data= Collections.synchronizedList(Database.getInstance().getAllCategories());
		response.setContentType("application/json");
		String jsonComposto=null;
		Gson gs= new Gson();
		synchronized (data) {
			jsonComposto= gs.toJson(data);
			response.getWriter().append(jsonComposto);
		}
	}
}
