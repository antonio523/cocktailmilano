package servlet;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import database.Database;
import model.HelperCocktail.Cocktail;


@WebServlet("/allCocktail.json")
public class AllCocktail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Cocktail> data= Collections.synchronizedList(Database.getInstance().getAllCocktails());
		response.setContentType("application/json");
		String jsonComposto=null;
		Gson gs= new Gson();
		synchronized (data) {
			jsonComposto= gs.toJson(data);
		response.getWriter().append(jsonComposto);
		}
	}
}
