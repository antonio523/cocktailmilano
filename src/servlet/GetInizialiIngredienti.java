package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import database.Database;

/**
 * Servlet implementation class GetInizialiIngredienti
 */
@WebServlet("/getInizialiIngredienti.json")
public class GetInizialiIngredienti extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> data= Database.getInstance().getInizialiIngredienti();
		response.setContentType("application/json");
		String jsonComposto=null;
		Gson gs= new Gson();
		jsonComposto= gs.toJson(data);
		response.getWriter().append(jsonComposto);
	}
}
